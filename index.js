// #3 - #4
const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`)

// #5 - #6
const address = ["1", "Molave Street", "South Forbes Park", "Makati City", "1220"]
const [houseNumber, street, villageName, cityName, postCode] = address
console.log(`I live at ${houseNumber}, ${street}, ${villageName}, ${cityName}, ${postCode}`)

// #7 - #8
const animalDetails = {
	animalName: "Lolong",
	animalType: "crocodile",
	specieType: "saltwater",
	animalWeight: 1075,
	animalMeasurement: "20 ft 3 in"
}
const { animalName, animalType, specieType, animalWeight, animalMeasurement } = animalDetails
function getAnimalDetails({animalName, animalType, specieType, animalWeight, animalMeasurement}) {
	console.log(`${animalName} was a ${specieType} ${animalType}. He weighed at ${animalWeight} kgs with a measurement of ${animalMeasurement}.`)
}
getAnimalDetails(animalDetails)

// #9 - #10
const numbersArray = [1,2,3,4,5]

numbersArray.forEach((numbersArray) => {
	console.log(numbersArray)
})

// #11 - #12
class Dog {
	constructor(name, age, breed) {
		this.name = name,
		this.age = age,
		this.breed = breed
	}
}
const myDog = new Dog ("Frankie", 5, "Miniature Dachshund")
console.log(myDog)